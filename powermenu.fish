#!/usr/bin/fish
set ACTION $(printf "Power Off\nRestart\nSleep\nLog Out" | bemenu --list 4 -c --width-factor 0.06 --accept-single -p "Power Opt" --fn "RobotoMono 18" --single-instance --cf "#18181800" -B 2 --no-spacing --tf "#464646")
echo $ACTION

switch $ACTION;
    case "Sleep"
        systemctl suspend
    case "Power Off"
        systemctl poweroff
    case "Restart"
        systemctl reboot
    case "Log Out"
        hyprctl dispatch exit
    end
